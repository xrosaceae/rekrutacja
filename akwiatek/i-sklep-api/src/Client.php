<?php

namespace ISklepApi;

use ISklepApi\Exception\Exception;
use ISklepApi\Model\RequestResponse;
use ISklepApi\Request\AbstractRequest;

final class Client
{
    const LOCAL_URI = 'http://rekrutacja.localhost:8091/';
    const PROD_URI = 'http://api.i-sklep.pl/';
    const URI_SUFIX = 'rest_api/shop_api/';
    const API_V1 = 'v1/';

    private $username;
    private $password;
    private $base_uri;
    private $curl;

    public function __construct(string $username, string $password, bool $local = false, string $version = self::API_V1)
    {
        $this->username = $username;
        $this->password = $password;

        if ($local)
            $this->base_uri = self::LOCAL_URI.self::URI_SUFIX.$version;
        else
            $this->base_uri = self::PROD_URI.self::URI_SUFIX.$version;
    }

    public function send(AbstractRequest $request): RequestResponse
    {
        try {
            $this->curl = curl_init();
            $uri = $this->base_uri.$request->getUri();
            curl_setopt($this->curl, CURLOPT_URL, $uri);
            curl_setopt($this->curl, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $request->getTimeout());
            switch ($request->getHttpMethod()) {
                case AbstractRequest::HTTP_GET:
                    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'GET');
                    break;
                case AbstractRequest::HTTP_POST:
                    curl_setopt( $this->curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
                    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $request->getParams());
                    break;
            }
            $response = curl_exec($this->curl);

            if (curl_errno($this->curl)) {
                throw new Exception('Error: '.curl_error($this->curl));
            } else {
                $status_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
                return $request->parseResponse($response,$status_code);
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}


