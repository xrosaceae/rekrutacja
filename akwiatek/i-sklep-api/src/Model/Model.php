<?php

namespace ISklepApi\Model;

class Model
{
    protected $is_saved = false;

    public function __construct()
    {

    }

    public function __get($name) {
        try {
            if (isset($this->$name)) {
                return $this->$name;
            } else {
                throw new \Exception("There's no such property: $name");
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function __set($name, $value) {
        try {
            if (isset($this->$name) && $this->is_saved === true) {
                throw new \Exception("Modifying not allowed: $name");
            } elseif (isset($this->$name) === false) {
                throw new \Exception("There's no such property: $name");
            } else {
                $this->$name=$value;
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function save(): void
    {
        $this->is_saved = true;
    }
}