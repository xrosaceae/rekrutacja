<?php

namespace ISklepApi\Model;

class Producer extends Model
{
    public $id;
    public $name;
    public $site_url;
    public $logo_filename;
    public $ordering;
    public $source_id;
}