<?php

namespace ISklepApi\Model;

class RequestError extends Model
{
    public $reason_code;
    public $messages;
}