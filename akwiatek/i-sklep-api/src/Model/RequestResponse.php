<?php

namespace ISklepApi\Model;

class RequestResponse extends Model
{
    public $status_code = 200;
    public $success = true;
    public $data = null;
    public $error = null;
}