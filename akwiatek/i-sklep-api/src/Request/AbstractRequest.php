<?php

namespace ISklepApi\Request;

use ISklepApi\Model\RequestResponse;

abstract class AbstractRequest
{
    const HTTP_GET = 'GET';
    const HTTP_PUT = 'PUT';
    const HTTP_POST = 'POST';
    const HTTP_DELETE = 'DELETE';

    protected $timeout = 3;
    protected $uri;
    protected $http_method;
    protected $method_called;
    protected $params;

    public function __construct()
    {

    }

    abstract function parseResponse($response, $status_code): RequestResponse;

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setTimeout($timeout): int
    {
        $this->timeout = $timeout;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getHttpMethod(): string
    {
        return $this->http_method;
    }
}