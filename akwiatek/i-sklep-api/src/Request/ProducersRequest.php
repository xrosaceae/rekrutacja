<?php

namespace ISklepApi\Request;

use ISklepApi\Model\Producer;
use ISklepApi\Model\RequestError;
use ISklepApi\Model\RequestResponse;

class ProducersRequest extends AbstractRequest
{
    public function parseResponse($response, $status_code): RequestResponse
    {
        $result = new RequestResponse();
        $response = json_decode($response);

        if ($status_code == 200) {
            $result->success = $response->success;
            switch ($this->method_called) {
                case 'createOne':
                    $data = $response->data->producer;
                    $producer = new Producer();
                    $producer->id = $data->id;
                    $producer->source_id = $data->source_id;
                    $producer->ordering = $data->ordering;
                    $producer->logo_filename = $data->logo_filename;
                    $producer->site_url = $data->site_url;
                    $producer->name = $data->name;
                    $producer->save();
                    $result->data = $producer;
                    break;
                case 'getAll':
                    $producers = array();
                    foreach ($response->data->producers as $item) {
                        $producer = new Producer();
                        $producer->id = $item->id;
                        $producer->source_id = $item->source_id;
                        $producer->ordering = $item->ordering;
                        $producer->logo_filename = $item->logo_filename;
                        $producer->site_url = $item->site_url;
                        $producer->name = $item->name;
                        $producer->save();
                        array_push($producers,$producer);
                    }
                    $result->data = $producers;
                    break;
            }
        } else {
            $result->status_code = $status_code;
            $result->data = $response->data;
            $result->success = $response->success;
            $error = new RequestError();
            $error->reason_code = $response->error->reason_code;
            $error->messages = $response->error->messages;
            $error->save();
            $result->error = $error;
        }

        $result->save();

        return $result;
    }

    public function createOne(Producer $producer): void
    {
        $this->http_method = self::HTTP_POST;
        $this->uri = 'producers/';
        $this->params = json_encode($producer);
        $this->method_called='createOne';
    }

    public function getAll(): void
    {
        $this->http_method = self::HTTP_GET;
        $this->uri = 'producers/';
        $this->method_called = 'getAll';
    }
}